import './App.css';
import React, {createRef, useState} from 'react';

function App() {

    let textInput = createRef();  // React use ref to get input value
    let [message,setMessage] = useState(["En attente..."]);

    let onOnclickHandler = (e) => {
        console.log(textInput.current.value);
        /// copie de l'ancien tableau dans le nouveau
        let newMessage = [...message];
        newMessage.push(textInput.current.value);
        setMessage(newMessage);
        textInput.current.value = "";
    };

    let onOnclickHandlerClear = (e) => {
      setMessage([]);
      textInput.current.value = "";
    };

    return (
        <div className="App">
            <h1> Hello, Tati! </h1>
            <input ref={textInput} type="text" />
            <button onClick={onOnclickHandler}>Click Here</button>
            <button onClick={onOnclickHandlerClear}>Clear</button>
            <h2>{message.map((element,index) => 
              <p key={index}>{element}</p>
            )}</h2>
        </div>
    );
}

export default App;
